# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository holds the RDLs and SPROCs for the original custom IES reports. These should never be needed, but in the off chance that they are,
everything needed to get them up and running again is contained here.

Bitbucket doesn't have RDL listed in it's syntax mode, so the RDLs will have XML listed as their language. When saving them to the network, be sure to
save them as a .rdl to ensure SSRS will recognize them properly.