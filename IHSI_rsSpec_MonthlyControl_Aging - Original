USE [TX_BSW_CIP]
GO

/****** Object:  StoredProcedure [dbo].[IHSI_rsSpec_MonthlyControl_Aging]    Script Date: 10/2/2018 11:42:06 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[IHSI_rsSpec_MonthlyControl_Aging]
@ProviderFilter VarChar(8) = ''
--@PostingPeriod char(6)
        
AS
/*
Description:  As of 4/19/2017, this sproc runs the top "aging" portion of the "Monthly Control" report in RS created for IES, CRMC, and Baylor.  
    This report was a combination of the Annual Activity and Aging by DOS reports.  
    
    NOTE:  This Aging procedure will NOT tie with the bottom section on the Monthly Control report if there 
            are services at a status 40 with a balance.  Ensure the "Service Status Code" report is 
            cleaned up!!!  If not, there's a line commented out in the "on hold" section below you can use to test with.
    
Author: Jeff Kunkel - original auther of the Annual Activity report.  Modified by Cheryl on 4/19/2017 for the Monthly Control.
Last Modified: 
    - 9/29/2017 by Cheryl per CIP/IES - They want to see all payments regardless if the service is on hold.  
        These changes are linked to the change Justin made to the other IHSI_rsSpec_MonthlyControl sproc on 9/25/2017.
        
    - 5/17/2017 by Cheryl and Justin per Rick on Zen #40477 - 
        1) Removed the status 99 section        
            This report ties with the Payment Analysis by Payor Type report if you filter that report to only show services NOT on hold.
These modifications below were made to the original Annual Activity report, not this new Monthly Control.
    - 6/12/2013 by Cheryl per SR 4215.  In preparation for the Cortex upgrade, I updated the field lengths where applicable from char(7) to varchar(9).
    - 8/3/2012 by Cheryl per SR 7870
        1) Updated the provider filtering section to allow for the new provider groups in the IHSI_ProviderGroup and IHSI_ProviderGroupAssignment tables.
        2) This sproc will soon replace the IHSI_rsSpec_AnnualActivity_Internal procedure.
        3) Improved performance of SQL by not using the DELETE statement for the tmp table and moving it back to the initial query's WHERE clause. 
    
    - 7/16/2012 by CW per Christie on SR 7718 - Added new fields: Contracted Adj, Collection Adj, Other Adj, Balance - Primary Ins, and Balance Secondary Ins.  
        1) Added the Adjustment.Number to the first section of code to ensure that adj's with the same amount on the same service don't get grouped as one adj.
            The payment section appears to be ok without adding the payment.number field. 
        2) Removed uneccessary joins to the first section of code.
    
    - 5/17/2012 by CW per Trish on SR 7562.  The Billing, Servicing & Payor Type breakdowns were not multiplying the s.amount times the s.units previously. I have corrected the code.
    - 10/14/2008 - Modified SQL flow for better performance.
    - 4/28/2007 - Modified for dynamic sql use and for provider filters.
    
*/
/*
----------- Variables -----------
@ProviderType = 'A' --Entire Database
@ProviderType = 'G' --Provider Group
@ProviderType = 'P' --Single Provider
------------------------------
*/
------------------ TESTING -------------------
--DECLARE
--@ProviderFilter VarChar(8)
----@PostingPeriod char(6) --not in use b/c it may not be possible for to gather the aging.
--SET @ProviderFilter = '' --All Providers
----SET @ProviderFilter = 'G1' --Group Provider format
----SET @ProviderFilter = 'P0000001' --Single Provider format
----SET @PostingPeriod = '201704'  --1 = Run for the last three years / 2 = All Years
---------------------------------------------
DECLARE @cSQL nvarchar(4000), @ProviderType Char(1), @ProviderFilter1 nvarchar(250)
--SET @StartDate = CAST(CAST(YEAR(GETDATE()) - 2 AS CHAR(4)) + '0101' AS smalldatetime) --gets January 1st three years back
--SET @EndDate = CAST(CAST(YEAR(GETDATE()) + 1 AS CHAR(4)) + '0101' AS smalldatetime) --gets January 1st next year
SET @ProviderType = LEFT(@ProviderFilter, 1)
        
SELECT @ProviderFilter1 = 
CASE
    WHEN @ProviderType = 'G' THEN ' AND s.ProviderNumber IN (  
        SELECT ProviderNumber 
        FROM IHSI_ProviderGroupAssignment 
        WHERE GroupNumber = SUBSTRING(''' + @ProviderFilter + ''' , 2, datalength(''' + @ProviderFilter + ''')) 
        )'
    WHEN @ProviderType = 'P' THEN ' AND s.ProviderNumber = SUBSTRING(''' + @ProviderFilter + ''', 2, datalength(''' + @ProviderFilter + '''))'
    ELSE '' 
END
-- Create temporary holding spot(s) for data
CREATE TABLE #tmpDataSet1 (
    Number varchar(9),
    PatientNumber varchar(9),
    PayorNumber Char(7),
    FromDate smalldatetime,
    Amount float, -- = Amount * Units
    ServiceStatusCode smallint,
    ClaimNumber char(9),
    StatementNumber char(9),
    SvcAdjustmentAmount float,
    PmtAmount Float,
    PmtAdjustmentAmount Float
    )
BEGIN 
-- Gather the Charges and Service Adjustments
SET @cSQL = '
    SELECT s.number, 
        s.patientnumber, 
        s.payornumber, 
        s.fromdate, 
        (s.Amount * s.units) as Amount,
        s.ServiceStatusCode,
        s.claimnumber,
        s.statementnumber,
        ISNULL(s.ServiceAdjustmentAmount, 0.00) as SvcAdjustmentAmount, 
        '''' as PmtAmount,
        '''' as PmtAdjustmentAmount
    FROM dbo.IHSI_MGTServices s 
    WHERE (ISNUMERIC(s.serviceitemcode) <> 0 OR s.serviceitemcode NOT IN (''' + '7XXXX' + ''', ''' + '8XXXX' + ''')) 
        --AND s.amount <> 0.00 
        AND s.servicestatuscode <> ''99'' --Added 9/29/2017 by Cheryl to exclude services and adjustments for services that are on hold, but allow any payments to be appear on report.
        ' + @ProviderFilter1 + '
    '
INSERT #tmpDataSet1
EXECUTE sp_executesql @cSQL
-- Gather the Payments
    SET @cSQL = 'SELECT s.number, 
    s.patientnumber, 
    s.payornumber, 
    s.fromdate, 
    ''0'' as Amount, 
    s.ServiceStatusCode,
    s.claimnumber,
    s.statementnumber,
    ''0'' as SvcAdjustmentAmount, 
    --SUM(ISNULL(p.PayAmount, 0.00) + ISNULL(p.AdjAmount, 0.00)) as PmtAmount
    SUM(ISNULL(p.PayAmount, 0.00)) as PmtAmount,
    SUM(ISNULL(p.AdjAmount, 0.00)) as PmtAdjustmentAmount
FROM dbo.IHSI_MGTServices s 
    LEFT JOIN dbo.IHSI_MGTPayments p ON s.number = p.SvcNumber
    LEFT JOIN dbo.Admission a WITH(NOLOCK) ON s.AdmissionNumber = a.Number
    LEFT JOIN dbo.payor pay WITH(NOLOCK) ON pay.number = p.pmtpayor
WHERE (ISNUMERIC(s.serviceitemcode) <> 0 OR s.serviceitemcode NOT IN (''' + '7XXXX' + ''', ''' + '8XXXX' + ''')) 
    --AND s.amount <> 0.00 
    ' + @ProviderFilter1 + '
GROUP BY s.number, 
    s.patientnumber, 
    s.payornumber, 
    s.fromdate, 
    s.ServiceStatusCode,
    s.claimnumber,
    s.statementnumber
    '
INSERT #tmpDataSet1
EXECUTE sp_executesql @cSQL
------------------------------------------------------------
/* This is the section where the aging dates are gathered */
------------------------------------------------------------
-- Create the temp table for the next set of results
SELECT 'Incomplete' AS GroupDesc,
    s.number AS ServiceNumber,      -- Start With Incompletes
    s.PatientNumber,
    s.PayorNumber,
    --s.ProviderNumber,
    p.TypeCode As TypeCode,
    s.FromDate AS AgingDate,
    s.Amount,
    s.SvcAdjustmentAmount,
    s.PmtAmount,
    s.PmtAdjustmentAmount
INTO dbo.#tmpSubSetMaster
FROM dbo.#tmpDataSet1 As s
    LEFT JOIN dbo.Payor AS p WITH(NOLOCK) ON s.PayorNumber = p.Number
WHERE s.servicestatuscode = 1
UNION               -- Bill Primary
(SELECT 'Aging' AS GroupDesc, 
    s.number AS ServiceNumber,
    s.PatientNumber,
    s.PayorNumber,
    --s.ProviderNumber,
    p.TypeCode As TypeCode,
    s.FromDate AS AgingDate,
    s.Amount,
    s.SvcAdjustmentAmount,
    s.PmtAmount,
    s.PmtAdjustmentAmount
FROM dbo.#tmpDataSet1 As s
    LEFT JOIN dbo.Payor AS p WITH(NOLOCK) ON s.PayorNumber = p.Number
WHERE s.servicestatuscode = 3)
UNION               -- Bill Secondary
(SELECT 'Aging' AS GroupDesc, 
    s.number AS ServiceNumber,
    s.PatientNumber,
    s.PayorNumber,
    --s.ProviderNumber,
    p.TypeCode As TypeCode,
    s.FromDate AS AgingDate,
    s.Amount,
    s.SvcAdjustmentAmount,
    s.PmtAmount,
    s.PmtAdjustmentAmount
FROM dbo.#tmpDataSet1 As s
    LEFT JOIN dbo.Payor AS p WITH(NOLOCK) ON p.Number = s.PayorNumber
WHERE s.servicestatuscode = 4)
UNION               -- Bill Patient
(SELECT 'Aging' AS GroupDesc,
    s.number AS ServiceNumber,
    s.PatientNumber,
    s.PayorNumber,
    --s.ProviderNumber,
    p.TypeCode As TypeCode,
    s.FromDate AS AgingDate,
    s.Amount,
    s.SvcAdjustmentAmount,
    s.PmtAmount,
    s.PmtAdjustmentAmount
FROM dbo.#tmpDataSet1 As s
    LEFT JOIN dbo.Payor AS p WITH(NOLOCK) ON s.PayorNumber = p.Number
--WHERE s.servicestatuscode = 7)  --Removed on 8/9/2013 by Cheryl per Trish.
WHERE s.servicestatuscode in ('7', '13'))  --Added on 8/9/2013 by Cheryl per Trish. This is for the OLOL and JPS Uninsured Patient Programs to pick up status 13's
UNION               -- Credit Balance
(SELECT 'Credit Balance' AS GroupDesc,
    s.number AS ServiceNumber,
    s.PatientNumber,
    s.PayorNumber,
    --s.ProviderNumber,
    p.TypeCode As TypeCode,
    s.FromDate AS AgingDate,
    s.Amount,
    s.SvcAdjustmentAmount,
    s.PmtAmount,
    s.PmtAdjustmentAmount
FROM dbo.#tmpDataSet1 As s
    LEFT JOIN dbo.Payor AS p WITH(NOLOCK) ON s.PayorNumber = p.Number
WHERE s.servicestatuscode IN (5,50))
----5/16/2017: Removed by Cheryl per Rick on ticket 
----9/29/2017: Added back by Cheryl in order to include any payments for services on hold, but exclude the charges and adjustments linked to those services.
UNION               -- Services On Hold
(SELECT 'On Hold' AS GroupDesc,
    s.number AS ServiceNumber,
    s.PatientNumber,
    s.PayorNumber,
    --s.ProviderNumber,
    p.TypeCode As TypeCode,
    s.FromDate AS AgingDate,
    s.Amount,
    s.SvcAdjustmentAmount,
    s.PmtAmount,
    s.PmtAdjustmentAmount
FROM dbo.#tmpDataSet1 As s
    LEFT JOIN dbo.Payor AS p WITH(NOLOCK) ON s.PayorNumber = p.Number
WHERE s.servicestatuscode = 99)
--WHERE s.servicestatuscode in ('40', '99')) --testing only: If there are servies at a status 40 with a balance, this line will help confirm that those accounts are causing the aging section to not tie with the bottom section. To fix the issue, someone needs to fix any accts that appear on the Service Status Code report that fit this scenario.
UNION               -- Other/Unknown Status
(SELECT 'Other' AS GroupDesc,
    s.number AS ServiceNumber,
    s.PatientNumber,
    s.PayorNumber,
    --s.ProviderNumber,
    p.TypeCode As TypeCode,
    s.FromDate AS AgingDate,
    s.Amount,
    s.SvcAdjustmentAmount,
    s.PmtAmount,
    s.PmtAdjustmentAmount
FROM dbo.#tmpDataSet1 As s
    LEFT JOIN dbo.Payor AS p WITH(NOLOCK) ON s.PayorNumber = p.Number
--WHERE (s.servicestatuscode IN (0,8)) OR (s.servicestatuscode = 27 AND s.Statementnumber is null and s.claimnumber is null))  --original line: removed on 3/24/2011
WHERE (s.servicestatuscode IN (0,8))    --updated on 3/24/2011 by CW - these 3 lines replaced the line above.
    OR (s.servicestatuscode = 27 AND ISNULL(s.statementnumber, '') = '') 
    OR (s.servicestatuscode in (23,24) AND ISNULL(s.claimnumber, '') = '') )
UNION               -- Waiting for Primary Payment
(SELECT 'Aging' AS GroupDesc,
    s.number AS ServiceNumber,
    s.PatientNumber,
    s.PayorNumber,
    --s.ProviderNumber,
    p.TypeCode As TypeCode,
    s.FromDate AS AgingDate,
    s.Amount,
    s.SvcAdjustmentAmount,
    s.PmtAmount,
    s.PmtAdjustmentAmount
FROM dbo.#tmpDataSet1 As s
    LEFT JOIN dbo.Payor AS p WITH(NOLOCK) ON s.PayorNumber = p.number
WHERE s.servicestatuscode = 23 
    AND ISNULL(s.claimnumber, '')  <> '' )   --updated on 3/24/2011 by CW from "AND s.claimmnumber IS NOT NULL"
UNION               -- Waiting for Secondary Payment
(SELECT 'Aging' AS GroupDesc,
    s.number AS ServiceNumber,
    s.PatientNumber,
    s.PayorNumber,
    --s.ProviderNumber,
    p.typecode As TypeCode,
    s.FromDate AS AgingDate,
    s.Amount,
    s.SvcAdjustmentAmount,
    s.PmtAmount,
    s.PmtAdjustmentAmount
FROM dbo.#tmpDataSet1 As s
    LEFT JOIN dbo.Payor As p WITH(NOLOCK) ON p.number = s.payornumber
WHERE s.servicestatuscode = 24 
    AND ISNULL(s.claimnumber, '')  <> '' )   --updated on 3/24/2011 by CW from "AND s.claimmnumber IS NOT NULL"
UNION               -- Waiting For Patient Payment
(SELECT 'Aging' AS GroupDesc,
    s.number AS ServiceNumber,
    s.PatientNumber,
    '0000001' AS PayorNumber,
    --s.ProviderNumber,
    'A' As TypeCode,
    s.FromDate AS AgingDate,
    s.Amount,
    s.SvcAdjustmentAmount,
    s.PmtAmount,
    s.PmtAdjustmentAmount
FROM dbo.#tmpDataSet1 As s
WHERE s.servicestatuscode = 27 
    AND ISNULL(s.statementnumber, '')  <> '')   --updated on 3/24/2011 by CW from "AND s.statementmnumber IS NOT NULL AND s.claimnumber IS NOT NULL)"
-- Now gather the patient information
SELECT s.GroupDesc,
    s.ServiceNumber,
    s.PatientNumber,
    s.PayorNumber,
    rtrim(s.TypeCode) as TypeCode,
    s.AgingDate,
    s.Amount,
    s.SvcAdjustmentAmount,
    s.PmtAmount,
    s.PmtAdjustmentAmount,
    p.Name As PatientName, 
    py.Name As PayorName,
    pt.Description As PayorType
    --pv.OrganizationName
FROM dbo.#tmpSubSetMaster AS s
    LEFT JOIN dbo.Patient As p WITH(NOLOCK) ON s.patientnumber = p.number
    LEFT JOIN dbo.Payor AS py WITH(NOLOCK) ON s.payornumber = py.number
    LEFT JOIN dbo.IHSI_PayorType As pt WITH(NOLOCK) ON s.typecode = pt.code
    --LEFT JOIN dbo.Provider AS pv ON s.ProviderNumber = pv.number
ORDER BY s.GroupDesc, s.TypeCode, s.PayorNumber, s.PatientNumber, s.Servicenumber
--ORDER BY s.ProviderNumber, s.GroupDesc, s.TypeCode, s.PayorNumber, s.PatientNumber, s.Servicenumber
--SET NOCOUNT OFF
end
-- Clean up temporary tables...
DROP TABLE #tmpDataSet1
DROP TABLE dbo.#tmpSubSetMaster
--select * from #tmpDataSet1 order by patientnumber
GO


